package com.shashi.test;

import com.shashi.entity.ProductEntity;
import com.shashi.vo.ProductVO;

public class ProductData {
	private ProductData() {
		
	}
	public static ProductVO create() {
		return new ProductVO(null,"HP Laptop", "Hp i7 dual core");
	}
	public static ProductEntity createEntity() {
		ProductEntity entity = new ProductEntity();
		entity.setProductName("Hp Laptop");
		entity.setDescription("Hp i7 dual core");
		entity.setId(2000L);
		return entity;
		
	}
}
